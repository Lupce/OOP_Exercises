<?php
require_once('avtor.php');
require_once('Knigi.php');
require_once('isbn.php');

$kniga1 = new Knigi('The Alchemist', 'March 1988');
$kniga2 = new Knigi(' The Pilgrimage','May 1987');

$avtor = new Avtor('Paulo Coelho','Paulo@gmail.com','Maski');

$isbn1 = new ISBN (12558, $kniga1);
$isbn2 = new ISBN (22588, $kniga2);

$avtor ->addNewIsbn($isbn1);
$avtor ->addNewIsbn($isbn2);

$avtor -> printInfoAvtor();


?>