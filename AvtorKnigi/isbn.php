<?php

require_once ('Knigi.php');

class ISBN{

 public $isbnbroj;
 public $knigi; // objekt od klasata Knigi*

 public function __construct(int $isbn, Knigi $kniga){

    $this ->isbnbroj = $isbn;
    $this ->knigi = $kniga;

   }

   public function printIsbn(){

    echo '<br> ISBN broj na knigata : '.$this ->isbnbroj.'<br>';
    $this ->knigi -> printKnigi(); // Objekt od klasa Knigi povikuva funkcija* 

   }

}

?>