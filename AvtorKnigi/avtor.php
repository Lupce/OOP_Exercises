<?php

// Pretpostavete deka ima aplikacija koja menadzira avtori i knigi. Za avtorot se cuva ime, email i pol. Za knigata se cuva ime, isbn kod, datum na izdavanje.
//Eden avtor moze da napisal poveke knigi.
//Na kraj da se kreira avtor i za nego da se ispecatat site informacii i za site negovi knigi.

//Prosirete ja zadacata na toj nacin sto za knigata ke se cuva izdavac, za koj so se cuvaat informacii za ime i adresa.

require_once ('isbn.php');

class Avtor{

   public $ime;
   public $email;
   public $pol;
   public $isbn; // Ova e niza od poveke broevi

   public function __construct($ime, $email, $pol, $isbn=[]){

     $this ->ime = $ime;
     $this ->email = $email;
     $this ->pol = $pol;
     $this ->isbn = $isbn;
    }

   public function printInfoAvtor(){
    echo'<br>';
    echo 'Imeto na avtorot e: '.$this ->ime.'<br>';
    echo 'Elektronska posta : '.$this ->email.'<br>';
    echo 'Pol: '.$this ->pol.'<br>';

    foreach ($this ->isbn as $isbn){

        $isbn->printIsbn();
        echo'<br>';

    }
 }

   public function addNewIsbn(ISBN $isbn){

    array_push($this ->isbn , $isbn);

   }

}

?>